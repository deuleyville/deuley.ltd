/**
 * Wave Text Animation
 * Looks for text between tilde characters (~like this~) and animates each character
 * with an organic, random wave motion
 */
document.addEventListener('DOMContentLoaded', function() {
  console.log('Original-style wave text animation loaded');
  
  // Check if user has reduced motion preference
  const prefersReducedMotion = window.matchMedia('(prefers-reduced-motion: reduce)').matches;
  console.log('Prefers reduced motion:', prefersReducedMotion);
  
  if (prefersReducedMotion) {
    console.log('Reduced motion enabled, not animating');
    return;
  }
  
  // Find all elements that might contain text with tildes
  const allElements = document.querySelectorAll('body *');
  
  allElements.forEach(element => {
    // Skip script and style elements
    if (element.tagName === 'SCRIPT' || element.tagName === 'STYLE') return;
    
    // Process each child node
    Array.from(element.childNodes).forEach(node => {
      // Only process text nodes
      if (node.nodeType === Node.TEXT_NODE && node.nodeValue.includes('~')) {
        const content = node.nodeValue;
        const fragment = document.createDocumentFragment();
        let lastIndex = 0;
        
        // Find all instances of text between tildes
        const regex = /~([^~]+)~/g;
        let match;
        
        while ((match = regex.exec(content)) !== null) {
          // Text before the match
          if (match.index > lastIndex) {
            fragment.appendChild(document.createTextNode(content.substring(lastIndex, match.index)));
          }
          
          // The opening tilde
          fragment.appendChild(document.createTextNode('~'));
          
          // Create a wrapper with proper ARIA attributes for accessibility
          const wrapper = document.createElement('span');
          wrapper.setAttribute('aria-label', match[1]);
          wrapper.className = 'wavy-text-wrapper';
          
          // The text between tildes with animation
          const text = match[1];
          // Split by character but preserve whitespace
          const chars = [...text];
          chars.forEach((char, i) => {
            if (char === ' ') {
              // Add space as a text node
              wrapper.appendChild(document.createTextNode(' '));
            } else {
              const charSpan = document.createElement('span');
              charSpan.className = 'wavy-char';
              charSpan.textContent = char;
              charSpan.setAttribute('aria-hidden', 'true');
              
              // Animation properties with longer delay between characters
              const delay = i * 0.5 + (Math.random() * 0.2);
              const duration = 2.8 + (Math.random() * 0.6);
              charSpan.style.animationDelay = `${delay}s`;
              charSpan.style.animationDuration = `${duration}s`;
              
              wrapper.appendChild(charSpan);
            }
          });
          
          fragment.appendChild(wrapper);
          
          // The closing tilde
          fragment.appendChild(document.createTextNode('~'));
          
          lastIndex = match.index + match[0].length;
        }
        
        // Any remaining text
        if (lastIndex < content.length) {
          fragment.appendChild(document.createTextNode(content.substring(lastIndex)));
        }
        
        // Replace the original text node
        element.replaceChild(fragment, node);
      }
    });
  });
  
  // Also handle elements with wavy-text class (for backward compatibility with our new approach)
  const wavyElements = document.querySelectorAll('.wavy-text');
  
  if (wavyElements.length) {
    console.log('Found wavy-text class elements:', wavyElements.length);
    
    wavyElements.forEach((element, elementIndex) => {
      // Skip if already processed
      if (element.querySelector('.wavy-char')) return;
      
      const text = element.textContent;
      if (!text || !text.trim()) return;
      
      // Clear the element
      element.textContent = '';
      
      // Create wrapper
      const wrapper = document.createElement('span');
      wrapper.setAttribute('aria-label', text);
      wrapper.className = 'wavy-text-wrapper';
      
      // Add individual characters with organic animation
      Array.from(text).forEach((char, i) => {
        if (char === ' ') {
          wrapper.appendChild(document.createTextNode(' '));
        } else {
          const span = document.createElement('span');
          span.textContent = char;
          span.className = 'wavy-char';
          span.setAttribute('aria-hidden', 'true');
          
          // Animation properties with longer delay between characters
          const delay = i * 0.5 + (Math.random() * 0.2);
          const duration = 2.8 + (Math.random() * 0.6);
          span.style.animationDelay = `${delay}s`;
          span.style.animationDuration = `${duration}s`;
          
          wrapper.appendChild(span);
        }
      });
      
      element.appendChild(wrapper);
    });
  }
}); 